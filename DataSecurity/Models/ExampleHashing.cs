﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSecurity.Models
{
    public class ExampleHashing
    {
        public string ClearText { get; set; }
        public string HashValue { get; set; }
    }
}