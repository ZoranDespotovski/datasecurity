﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSecurity.Models
{
    public class ExampleAsymmetricEncryption
    {
        public string ClearText { get; set; }
        public string KeyPublic { get; set; }
        public string ChicipherText { get; set; }
    }
}