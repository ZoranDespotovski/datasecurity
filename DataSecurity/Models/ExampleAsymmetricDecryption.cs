﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSecurity.Models
{
    public class ExampleAsymmetricDecryption
    {
        public string ChicipherText { get; set; }
        public string KeyPrivate { get; set; }
        public string ClearText { get; set; }
    }
}