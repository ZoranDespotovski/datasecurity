﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSecurity.Models
{
    public class ExampleAsymmetricEncryptionKeys
    {
        public int UserId { get; set; }
        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }
    }
}