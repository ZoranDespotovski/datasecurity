﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSecurity.Models
{
    public class ExampleSymmetricDecryption
    {
        public string ChicipherText { get; set; }
        public string KeySymetric { get; set; }
        public string ClearText { get; set; }
    }
}