﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataSecurity.Models
{
    public class ExampleSecurity
    {
        public ExampleHashing ExampleHashing { get; set; }

        public ExampleSymmetricEncryption ExampleSymmetricEncryption { get; set; }
        public ExampleSymmetricDecryption ExampleSymmetricDecription { get; set; }

        public ExampleAsymmetricEncryptionKeys ExampleAsymmetricEncryptionKeys { get; set; }
        public ExampleAsymmetricEncryption ExampleAssAsymmetricEncryption { get; set; }
        public ExampleAsymmetricDecryption ExampleAssAsymmetricDecryption { get; set; }
    }
}