using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace DataSecurity.Helpers
{
    public class Encryption
    {
        public static readonly string RSAKeysDirectoryPath = ConfigurationManager.AppSettings.Get("RSAKeysDirectory");

        //   --- Hashing ---
        //Hash algorithms are one way functions, meaning: they turn any amount of data into a fixed-length checksum that cannot be reversed. They also have the property that if the input changes by even a tiny bit, the resulting hash is COMPLETELY different.
        // http://crackstation.net/hashing-security.html
        #region Hashing

        /// <summary>
        /// Hashes a password
        /// </summary>
        /// <param name="password">The password to hash</param>

        /// <returns>The hashed password as a 128 character hex string</returns>
        public static string HashPassword(string password)
        {
            string salt = GetRandomSalt();
            string hash = Sha256Hex(salt + password);
            return salt + hash;
        }

        /// <summary>
        /// Validates a password
        /// </summary>
        /// <param name="password">The password to test</param>
        /// <param name="correctHash">The hash of the correct password</param>
        /// <returns>True if password is the correct password, false otherwise</returns>
        public static bool HASHValidatePassword(string password, string correctHash)
        {
            if (correctHash.Length < 128)
                throw new ArgumentException("correctHash must be 128 hex characters!");
            string salt = correctHash.Substring(0, 64);
            string validHash = correctHash.Substring(64, 64);
            string passHash = Sha256Hex(salt + password);
            return string.Compare(validHash, passHash) == 0;
        }

        /// <summary>
        /// Returns the SHA256 hash of a string, formatted in hex
        /// </summary>
        /// <param name="toHash"> string to hash </param>
        /// <returns>hashed string</returns>
        private static string Sha256Hex(string toHash)
        {
            SHA256Managed hash = new SHA256Managed();
            byte[] utf8 = UTF8Encoding.UTF8.GetBytes(toHash);
            return BytesToHex(hash.ComputeHash(utf8));
        }

        /// <summary>
        /// Returns a random 64 character hex string (256 bits)
        /// </summary>
        /// <returns>Random hex string</returns>
        private static string GetRandomSalt()
        {
            RNGCryptoServiceProvider random = new RNGCryptoServiceProvider();
            byte[] salt = new byte[32]; //256 bits
            random.GetBytes(salt);
            return BytesToHex(salt);
        }

        /// <summary>
        /// Converts a byte array to a hex string
        /// </summary>
        /// <param name="toConvert">Byte array for converting</param>
        /// <returns>Hex string</returns>
        private static string BytesToHex(byte[] toConvert)
        {
            StringBuilder s = new StringBuilder(toConvert.Length * 2);
            foreach (byte b in toConvert)
            {
                s.Append(b.ToString("x2"));
            }
            return s.ToString();
        }

        #endregion


        //   --- Asymmetric encryption - RSA ---
        // Asymmetric encryption uses a related key-pair to encrypt and decrypt data. One of the keys is the “public key” and the other is the “private key”. The data encrypted with the public key can only be decrypted with the private key, and vice-versa. RSA is an algorithm for public-key cryptography that is based on the presumed difficulty of factoring large integers.
        // http://www.dotnetspider.com/resources/692-RSA-Encryption-C.aspx
        #region RSA

        public static RSACryptoServiceProvider rsa;
        //public static string RSApublicOnlyKeyXML;
        //public static string RSApublicPrivateKeyXML;

        /// <summary>
        /// Assigning and initializing the keys
        /// </summary>
        public static void RSAAssignParameter()
        {
            const int PROVIDER_RSA_FULL = 1;
            const string CONTAINER_NAME = "KeyContainer";
            CspParameters cspParams;

            cspParams = new CspParameters(PROVIDER_RSA_FULL);
            cspParams.KeyContainerName = CONTAINER_NAME;
            cspParams.Flags = CspProviderFlags.UseMachineKeyStore;
            cspParams.ProviderName = "Microsoft Strong Cryptographic Provider";
            rsa = new RSACryptoServiceProvider(cspParams);

            //RSApublicOnlyKeyXML = "";
            //RSApublicPrivateKeyXML = "";

            rsa.PersistKeyInCsp = false;
        }
        /// <summary>
        /// Assign new key pair for user
        /// </summary>
        /// <param name="userId">User for assigning public/private key pair</param>
        /// <param name="password">User's login password used for unique key generation</param>
        public static void RSAAssignNewKey(int userId)
        {
            RSAAssignParameter();

            var RSApublicPrivateKeyXML = rsa.ToXmlString(true);
            var RSApublicOnlyKeyXML = rsa.ToXmlString(false);
            
            //go kriptirame so AES
            //RSApublicPrivateKeyXML = AESgetCryptedString(RSApublicPrivateKeyXML, password);

            //write publicOnlyKeyXML(clear text) and publicPrivateKeyXML(encrypt with AES) in database
            //bool b = Functions.insertUsersPasswords_insertNewPublicPrivatePairForUser(userId, RSApublicOnlyKeyXML, RSApublicPrivateKeyXML);

            //provide public and private RSA params
            StreamWriter writer = new StreamWriter(string.Format("{0}{1}_privatekey.xml", RSAKeysDirectoryPath, userId));
            writer.Write(RSApublicPrivateKeyXML);
            writer.Close();

            //provide public only RSA params
            writer = new StreamWriter(string.Format("{0}{1}_publickey.xml", RSAKeysDirectoryPath, userId));
            writer.Write(RSApublicOnlyKeyXML);
            writer.Close();

            rsa.Clear();

        }
        /// <summary>
        /// Ussigning new fresh key pair for user
        /// </summary>
        /// <param name="userId">User for assigning new public/private key pair</param>
        /// <param name="password">User's new login password used for new unique key generation</param>
        public static void RSAAssignNewKeyUpdate(int userId, string password)
        {
            RSAAssignParameter();

            var RSApublicOnlyKeyXML = rsa.ToXmlString(false);
            var RSApublicPrivateKeyXML = rsa.ToXmlString(true);

            //go kriptirame so AES
            //RSApublicPrivateKeyXML = AESgetCryptedString(RSApublicPrivateKeyXML, password);

            //write publicOnlyKeyXML(clear text) and publicPrivateKeyXML(encrypt with AES) in database
            //bool b = Functions.updateUsersPasswords_updateNewPublicPrivatePairForUser(userId, RSApublicOnlyKeyXML, RSApublicPrivateKeyXML);

            //provide public and private RSA params
            StreamWriter writer = new StreamWriter(string.Format("{0}{1}_privatekey.xml", RSAKeysDirectoryPath, userId));
            string publicPrivateKeyXML = rsa.ToXmlString(true);
            writer.Write(publicPrivateKeyXML);
            writer.Close();

            //provide public only RSA params
            writer = new StreamWriter(string.Format("{0}{1}_publickey.xml", RSAKeysDirectoryPath, userId));
            string publicOnlyKeyXML = rsa.ToXmlString(false);
            writer.Write(publicOnlyKeyXML);
            writer.Close();

            rsa.Clear();

        }
        
        /// <summary>
        /// Encypting string with RSA
        /// <param name="data2Encrypt">String to encrypt</param>
        /// <param name="userId">User's password which is used as a public key for RSA encryption</param>
        /// <returns></returns>
        public static string RSAEncryptData(string data2Encrypt, string rsaPublicOnlyKeyXML)
        {
            //string RSApublicOnlyKeyXML = "";//Functions.selectUsersPasswords_getPublicKeyForUser(userId);

            if (rsaPublicOnlyKeyXML != "")
                RSAAssignParameter();
            else
            {
                return "";
                //RSAAssignNewKey(userId);
            }

            rsa.FromXmlString(rsaPublicOnlyKeyXML);

            //read plaintext, encrypt it to ciphertext
            byte[] plainbytes = null;
            byte[] cipherbytes = null;
            
            plainbytes = System.Text.Encoding.UTF8.GetBytes(data2Encrypt);
            cipherbytes = rsa.Encrypt(plainbytes, false);

            rsa.Clear();
            return Convert.ToBase64String(cipherbytes);
        }

        /// <summary>
        /// Decypting string with RSA
        /// </summary>
        /// <param name="data2Decrypt">String to decrypt</param>
        /// <param name="userPrivateKey">User's password which is used as a private key for RSA decryption</param>
        /// <returns></returns>
        public static string RSADecryptData(string data2Decrypt, string rsaPublicPrivateKeyXML)
        {
            RSAAssignParameter();

            byte[] getpassword = Convert.FromBase64String(data2Decrypt);
            rsa.FromXmlString(rsaPublicPrivateKeyXML);

            //read ciphertext, decrypt it to plaintext
            byte[] plain = null;

            plain = rsa.Decrypt(getpassword, false);

            rsa.Clear();
            return System.Text.Encoding.UTF8.GetString(plain);
        }

        #endregion

        //   --- Symmetric encryption - AES ---
        // Rijndael (pronounced rain-dahl) is the algorithm that has been selected by the U.S. National Institute of Standards and Technology (NIST) as the candidate for the Advanced Encryption Standard (AES).
        // http://www.obviex.com/samples/EncryptionWithSalt.aspx
        #region AES Rijndael

        /// <summary>
        /// Encrypting string
        /// </summary>
        /// <param name="plainText">text for encrypting</param>
        /// <param name="passPhrase">password for encryption</param>
        /// <param name="saltValue">Salted value </param>
        /// <param name="hashAlgorithm">Type of the algorithm used</param>
        /// <param name="passwordIterations">Number of iteration</param>
        /// <param name="initVector">Initial vector</param>
        /// <param name="keySize">KeySize</param>
        /// <returns>Encrypted ciphertext</returns>
        protected static string AESEncrypt(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            string cipherText = "";
            try
            {
                // Convert strings into byte arrays.
                // Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                // Convert our plaintext into a byte array.
                // Let us assume that plaintext contains UTF8-encoded characters.
                byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

                // First, we must create a password, from which the key will be derived.
                // This password will be generated from the specified passphrase and 
                // salt value. The password will be created using the specified hash 
                // algorithm. Password creation can be done in several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                                passPhrase,
                                                                saltValueBytes,
                                                                hashAlgorithm,
                                                                passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate encryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                                                                 keyBytes,
                                                                 initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream();

                // Define cryptographic stream (always use Write mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                             encryptor,
                                                             CryptoStreamMode.Write);
                // Start encrypting.
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                // Finish encrypting.
                cryptoStream.FlushFinalBlock();

                // Convert our encrypted data from a memory stream into a byte array.
                byte[] cipherTextBytes = memoryStream.ToArray();

                // Close both streams.
                memoryStream.Close();
                memoryStream.Flush();
                cryptoStream.Close();
                cryptoStream.Flush();

                // Convert encrypted data into a base64-encoded string.
                cipherText = Convert.ToBase64String(cipherTextBytes);
            }
            catch (Exception exc)
            { ; }

            // Return encrypted string.
            return cipherText;
        }
        /// <summary>
        /// Decrypting chipper text
        /// </summary>
        /// <param name="cipherText">Crypted string</param>
        /// <param name="passPhrase">password for encryption</param>
        /// <param name="saltValue">Salted value </param>
        /// <param name="hashAlgorithm">Type of the algorithm used</param>
        /// <param name="passwordIterations">Number of iteration</param>
        /// <param name="initVector">Initial vector</param>
        /// <param name="keySize">KeySize</param>
        /// <returns>Encrypted ciphertext</returns>
        protected static string AESDecrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            string plainText = "";

            try
            {
                // Convert strings defining encryption key characteristics into byte
                // arrays. Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);


                int mod4 = cipherText.Length % 4;
                if (cipherText.Length > 0)
                {
                    cipherText += new string('=', 4 - mod4);
                }
                // Convert our ciphertext into a byte array.
                byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

                // First, we must create a password, from which the key will be 
                // derived. This password will be generated from the specified 
                // passphrase and salt value. The password will be created using
                // the specified hash algorithm. Password creation can be done in
                // several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                                passPhrase,
                                                                saltValueBytes,
                                                                hashAlgorithm,
                                                                passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate decryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                                                                 keyBytes,
                                                                 initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

                // Define cryptographic stream (always use Read mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                              decryptor,
                                                              CryptoStreamMode.Read);

                // Since at this point we don't know what the size of decrypted data
                // will be, allocate the buffer long enough to hold ciphertext;
                // plaintext is never longer than ciphertext.
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                // Start decrypting.
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                // Close both streams.
                memoryStream.Flush();
                memoryStream.Close();
                cryptoStream.Flush();
                cryptoStream.Close();

                // Convert decrypted data into a string. 
                // Let us assume that the original plaintext string was UTF8-encoded.
                plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                           0,
                                                           decryptedByteCount);
            }
            catch (Exception exc)
            { ; }

            // Return decrypted string.
            return plainText;
        }

        /// <summary>
        /// Encrypting bytes
        /// </summary>
        /// <param name="fileInBytes">File for encrypting</param>
        /// <param name="passPhrase">Password for encryption</param>
        /// <param name="saltValue">Salted value </param>
        /// <param name="hashAlgorithm">Type of the algorithm used</param>
        /// <param name="passwordIterations">Number of iteration</param>
        /// <param name="initVector">Initial vector</param>
        /// <param name="keySize">KeySize</param>
        /// <returns>Encrypted bytes</returns>
        protected static byte[] AESEncryptFile(byte[] fileInBytes, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            byte[] cipherTextBytes = null;

            try
            {
                // Convert strings into byte arrays.
                // Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                // Convert our plaintext into a byte array.
                // Let us assume that plaintext contains UTF8-encoded characters.
                byte[] plainTextBytes = fileInBytes;

                // First, we must create a password, from which the key will be derived.
                // This password will be generated from the specified passphrase and 
                // salt value. The password will be created using the specified hash 
                // algorithm. Password creation can be done in several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                                passPhrase,
                                                                saltValueBytes,
                                                                hashAlgorithm,
                                                                passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate encryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
                                                                 keyBytes,
                                                                 initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream();

                // Define cryptographic stream (always use Write mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                             encryptor,
                                                             CryptoStreamMode.Write);
                // Start encrypting.
                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                // Finish encrypting.
                cryptoStream.FlushFinalBlock();


                // Convert our encrypted data from a memory stream into a byte array.
                cipherTextBytes = memoryStream.ToArray();

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();
            }
            catch (Exception exc)
            { ; }

            // Return encrypted string.
            return cipherTextBytes;
        }
        /// <summary>
        /// Decrypting bytes
        /// </summary>
        /// <param name="cipherFileInBytes">Encrypted bytes</param>
        /// <param name="passPhrase">Password for encryption</param>
        /// <param name="saltValue">Salted value </param>
        /// <param name="hashAlgorithm">Type of the algorithm used</param>
        /// <param name="passwordIterations">Number of iteration</param>
        /// <param name="initVector">Initial vector</param>
        /// <param name="keySize">KeySize</param>
        /// <returns>Decrypted bytes</returns>
        protected static byte[] AESDecryptFile(byte[] cipherFileInBytes, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
        {
            byte[] plainTextBytes = null;
            byte[] truePlainTextBytes = null;
            try
            {
                // Convert strings defining encryption key characteristics into byte
                // arrays. Let us assume that strings only contain ASCII codes.
                // If strings include Unicode characters, use Unicode, UTF7, or UTF8
                // encoding.
                byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
                byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

                // Convert our ciphertext into a byte array.
                byte[] cipherFileBytes = cipherFileInBytes;

                // First, we must create a password, from which the key will be 
                // derived. This password will be generated from the specified 
                // passphrase and salt value. The password will be created using
                // the specified hash algorithm. Password creation can be done in
                // several iterations.
                PasswordDeriveBytes password = new PasswordDeriveBytes(
                                                                passPhrase,
                                                                saltValueBytes,
                                                                hashAlgorithm,
                                                                passwordIterations);

                // Use the password to generate pseudo-random bytes for the encryption
                // key. Specify the size of the key in bytes (instead of bits).
                byte[] keyBytes = password.GetBytes(keySize / 8);

                // Create uninitialized Rijndael encryption object.
                RijndaelManaged symmetricKey = new RijndaelManaged();

                // It is reasonable to set encryption mode to Cipher Block Chaining
                // (CBC). Use default options for other symmetric key parameters.
                symmetricKey.Mode = CipherMode.CBC;

                // Generate decryptor from the existing key bytes and initialization 
                // vector. Key size will be defined based on the number of the key 
                // bytes.
                ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
                                                                 keyBytes,
                                                                 initVectorBytes);

                // Define memory stream which will be used to hold encrypted data.
                MemoryStream memoryStream = new MemoryStream(cipherFileBytes);

                // Define cryptographic stream (always use Read mode for encryption).
                CryptoStream cryptoStream = new CryptoStream(memoryStream,
                                                              decryptor,
                                                              CryptoStreamMode.Read);

                // Since at this point we don't know what the size of decrypted data
                // will be, allocate the buffer long enough to hold ciphertext;
                // plaintext is never longer than ciphertext.
                plainTextBytes = new byte[cipherFileBytes.Length];

                // Start decrypting.
                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

                truePlainTextBytes = new byte[decryptedByteCount];
                Array.Copy(plainTextBytes, truePlainTextBytes, decryptedByteCount);

                // Close both streams.
                memoryStream.Close();
                cryptoStream.Close();
            }
            catch (Exception exc)
            { ; }

            // Return decrypted string.
            return truePlainTextBytes;
        }

        /// <summary>
        /// Get chipertext
        /// </summary>
        /// <param name="clearText">Clear text</param>
        /// <param name="password">Password used for encryption/decryption</param>
        /// <returns>Chipertext</returns>
        public static string AESgetCryptedString(string clearText, string password)
        {
            string passPhrase = password;        // can be any string
            string saltValue = HashPassword(password);   // can be any string
            string hashAlgorithm = "SHA1";             // can be "MD5"
            int passwordIterations = 1;                  // can be any number
            string initVector = HashPassword(password).Substring(0, 16);      //"@1B2c3D4e5F6g7H8"; // must be 16 bytes
            //initVector = customIV;
            int keySize = 256;                // can be 192 or 128

            string plainText = clearText;    // original plaintext

            string encrypedText = "";
            
            encrypedText = Encryption.AESEncrypt(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            encrypedText += saltValue + initVector;

            return encrypedText;
        }

        /// <summary>
        /// Get decrypted cleartext
        /// </summary>
        /// <param name="cryptedText">Encrypted chipertext</param>
        /// <param name="password">Password used for encryption/decryption</param>
        /// <returns>Cleartext</returns>
        public static string AESgetDecryptedString(string cryptedText, string password)
        {
            string cipherText = cryptedText;    // original plaintext
            
            int dolzina = 16;
            int odIndex = cipherText.Length - dolzina;
            string initVector = cryptedText.Substring(odIndex, dolzina);
            dolzina = 128;
            odIndex = odIndex - dolzina;
            string saltValue = cipherText.Substring(odIndex, dolzina);

            string passPhrase = password;

            string hashAlgorithm = "SHA1";             // can be "MD5"
            int passwordIterations = 1;                  // can be any number
            int keySize = 256;                // can be 192 or 128


            cipherText = cipherText.Substring(0, cipherText.Length - (128 + 16 + 1));
            string clearText = "";
            clearText = Encryption.AESDecrypt(cipherText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

            return clearText;
        }

        /// <summary>
        /// Get encrypted bytes
        /// </summary>
        /// <param name="clearFile">Bytes for encrypting</param>
        /// <param name="password">Password used for encryption/decryption</param>
        /// <returns>Encrypted bytes</returns>
        public static byte[] AESgetCryptedFile(byte[] clearFile, string password)
        {
            string passPhrase = password;        // can be any string
            
            string saltValue = HashPassword(password);   // can be any string
            byte[] saltValueInBytes = System.Text.Encoding.Unicode.GetBytes(saltValue);
            
            string hashAlgorithm = "SHA1";             // can be "MD5"
            int passwordIterations = 1;                  // can be any number
            
            string initVector = HashPassword(password).Substring(0, 16);      //"@1B2c3D4e5F6g7H8"; // must be 16 bytes
            //initVector = customIV;
            byte[] initVectorInBytes = System.Text.Encoding.Unicode.GetBytes(initVector);
            
            int keySize = 256;                // can be 192 or 128

            byte[] plainFile = clearFile;    // original plaintext

            byte[] encrypedFile = null;
            encrypedFile = Encryption.AESEncryptFile(plainFile, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
            
            byte[] encrypedFileWithKeys = new byte[encrypedFile.Length + saltValueInBytes.Length + initVectorInBytes.Length];
            System.Buffer.BlockCopy(encrypedFile, 0, encrypedFileWithKeys, 0, encrypedFile.Length);
            System.Buffer.BlockCopy(saltValueInBytes, 0, encrypedFileWithKeys, encrypedFile.Length, saltValueInBytes.Length);
            System.Buffer.BlockCopy(initVectorInBytes, 0, encrypedFileWithKeys, encrypedFile.Length + saltValueInBytes.Length, initVectorInBytes.Length);

            return encrypedFileWithKeys;
        }

        /// <summary>
        /// Get decrypted bytes
        /// </summary>
        /// <param name="cryptedFile">Encrypted bytes for decrypting</param>
        /// <param name="password">Password used for encryption/decryption</param>
        /// <returns>Decrypted bytes</returns>
        public static byte[] AESgetDecryptedFile(byte[] cryptedFile, string password)
        {
            byte[] cipherFile = cryptedFile;    // original plaintext

            int dolzinaNaBajti = 32;
            int odIndex = cipherFile.Length - dolzinaNaBajti;

            //System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            //str = enc.GetString(dBytes);
            List<byte[]> tmp = SplitByteArray(cryptedFile, odIndex);
            cipherFile = tmp[0];
            byte[] initVectorInBytes = tmp[1];
            //string initVector = Convert.ToBase64String(initVectorInBytes);
            string initVector = System.Text.Encoding.Unicode.GetString(initVectorInBytes);

            dolzinaNaBajti = 256;
            odIndex = odIndex - dolzinaNaBajti;
            
            tmp.Clear();
            tmp = SplitByteArray(cipherFile, odIndex);
            cipherFile = tmp[0];
            byte[] saltValueInBytes = tmp[1];
            //string saltValue = Convert.ToBase64String(saltValueInBytes);
            string saltValue = System.Text.Encoding.Unicode.GetString(saltValueInBytes);

            string passPhrase = password;

            string hashAlgorithm = "SHA1";             // can be "MD5"
            int passwordIterations = 1;                  // can be any number
            int keySize = 256;                // can be 192 or 128

            
            byte[] clearFile = null;
            clearFile = Encryption.AESDecryptFile(cipherFile, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

            return clearFile;
        }


        /// <summary>
        /// Helper method for spliting byte array
        /// </summary>
        /// <param name="b">Bytes in array</param>
        /// <param name="FromWhere">Position for spliting</param>
        /// <returns>List of bytes in array</returns>
        protected static List<byte[]> SplitByteArray(byte[] b, int FromWhere)
        {
            if (FromWhere >= b.Length)
                throw new Exception("Error: FromWhere exceeds the length of the byte array.");
            else
            {
                List<byte[]> ReturnList = new List<byte[]>();

                byte[] tmp = b;
                Array.Resize(ref tmp, FromWhere);
                ReturnList.Add(tmp);

                tmp = new byte[b.Length - FromWhere];
                Array.Copy(b, FromWhere, tmp, 0, b.Length - FromWhere);
                ReturnList.Add(tmp);

                return ReturnList;
            }
        }

        /// <summary>
        /// Get file in bytes
        /// </summary>
        /// <param name="filePath">Path to the file</param>
        /// <returns>File in bytes</returns>
        public static byte[] getFileInBytes(string filePath)
        {
            byte[] fileInBytes = null;
            if (filePath == null)
                return null;

            System.IO.StreamReader streamReader = new System.IO.StreamReader(filePath);
            System.IO.BinaryReader br = new System.IO.BinaryReader(streamReader.BaseStream);

            fileInBytes = new byte[streamReader.BaseStream.Length];
            br.Read(fileInBytes, 0, (int)streamReader.BaseStream.Length);

            streamReader.Close();
            br.Close();

            return fileInBytes;
        }

        #endregion

    }
}