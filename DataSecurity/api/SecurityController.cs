﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataSecurity.Models;

namespace DataSecurity.api
{
    public class SecurityController : ApiController
    {
        public ExampleSecurity GetExampleSecurity(string clearText, string keySymmetric, int userId)
        {
            var hashValue = GetHashExample(clearText);
            var symmetricEncryptionValue = GetSymmetricEncryptionExample(clearText, keySymmetric);
            var symmetricDecryptionValue = GetSymmetricDecryptionExample(symmetricEncryptionValue.ChicipherText, keySymmetric);
            var asymmetricEncryptionKeys = ExampleAsymetricKeyGeneration(userId);
            var asymmetricEncryption = GetAsymetricEncryptionExample(clearText, userId);
            var asymmetricDecryption = GetAsymmetricDecryptionExample(asymmetricEncryption.ChicipherText, userId);

            return new ExampleSecurity
            {
                ExampleHashing = hashValue,
                ExampleSymmetricEncryption = symmetricEncryptionValue,
                ExampleSymmetricDecription = symmetricDecryptionValue,
                ExampleAsymmetricEncryptionKeys = asymmetricEncryptionKeys,
                ExampleAssAsymmetricEncryption = asymmetricEncryption,
                ExampleAssAsymmetricDecryption = asymmetricDecryption
            };
        }

        private ExampleHashing GetHashExample(string clearText)
        {
            return new ExampleHashing
            {
                ClearText = clearText,
                HashValue = Helpers.Encryption.HashPassword(clearText)
            };
        }
        private ExampleSymmetricEncryption GetSymmetricEncryptionExample(string clearText, string keySymmetric)
        {
            return new ExampleSymmetricEncryption
            {
                ClearText = clearText,
                KeySymetric = keySymmetric,
                ChicipherText = Helpers.Encryption.AESgetCryptedString(clearText, keySymmetric)
            };
        }
        private ExampleSymmetricDecryption GetSymmetricDecryptionExample(string chicipherText, string keySymmetric)
        {
            return new ExampleSymmetricDecryption
            {
                ChicipherText = chicipherText,
                KeySymetric = keySymmetric,
                ClearText = Helpers.Encryption.AESgetDecryptedString(chicipherText, keySymmetric)
            };
        }

        private ExampleAsymmetricEncryptionKeys ExampleAsymetricKeyGeneration(int userId)
        {
            var publicOnlyKeyXML = GetPublicOnlyKEY(userId);
            var publicPrivateKeyXML = GetPublicPrivateKey(userId);

            if (string.IsNullOrEmpty(publicOnlyKeyXML) || string.IsNullOrEmpty(publicPrivateKeyXML))
            {
                Helpers.Encryption.RSAAssignNewKey(userId);
                publicOnlyKeyXML = GetPublicOnlyKEY(userId);
                publicPrivateKeyXML = GetPublicPrivateKey(userId);
            }

            return new ExampleAsymmetricEncryptionKeys
            {
                UserId = userId,
                PublicKey = publicOnlyKeyXML,
                PrivateKey = publicPrivateKeyXML
            };
        }
        private string GetPublicOnlyKEY(int userId)
        {
            var publicKeyPath = string.Format("{0}{1}_publickey.xml", Helpers.Encryption.RSAKeysDirectoryPath, userId);
            if (!File.Exists(publicKeyPath))
                return "";

            StreamReader reader = new StreamReader(publicKeyPath);
            string publicOnlyKeyXML = reader.ReadToEnd();
            reader.Close();

            return publicOnlyKeyXML;
        }
        private string GetPublicPrivateKey(int userId)
        {
            var privateKeyPath = string.Format("{0}{1}_privatekey.xml", Helpers.Encryption.RSAKeysDirectoryPath, userId);
            if (!File.Exists(privateKeyPath))
                return "";

            StreamReader reader = new StreamReader(privateKeyPath);
            string publicPrivateKeyXML = reader.ReadToEnd();
            reader.Close();

            return publicPrivateKeyXML;
        }
        private ExampleAsymmetricEncryption GetAsymetricEncryptionExample(string clearText, int userId)
        {
            var publicOnlyKeyXML = GetPublicOnlyKEY(userId);

            return new ExampleAsymmetricEncryption
            {
                ClearText = clearText,
                KeyPublic = publicOnlyKeyXML,
                ChicipherText = Helpers.Encryption.RSAEncryptData(clearText, publicOnlyKeyXML)
            };
        }
        private ExampleAsymmetricDecryption GetAsymmetricDecryptionExample(string chipperText, int userId)
        {
            var privateKey = GetPublicPrivateKey(userId);

            return new ExampleAsymmetricDecryption
            {
                ChicipherText = chipperText,
                KeyPrivate = privateKey,
                ClearText = Helpers.Encryption.RSADecryptData(chipperText, privateKey)
            };
        }
    }
}
